// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig: {
    apiKey: "AIzaSyB5ZMtKdiYX6d3YM9ji6iyJAIrse1yAKa4",
    authDomain: "tpfirebaseangular.firebaseapp.com",
    databaseURL: "https://tpfirebaseangular-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "tpfirebaseangular",
    storageBucket: "tpfirebaseangular.appspot.com",
    messagingSenderId: "198068250928",
    appId: "1:198068250928:web:193d5cc4e2a3bf8d0680f5",
    measurementId: "G-DVZ51EQZS7"
  },

  firebaseMessage:{
    'auth/user-not-found': `Identifiants incorrect`,
    'auth/invalid-email': `Le champ email n'est pas correctement formatté`,
    'auth/wrong-password': `Mot de passe incorrect`,
    'auth/weak-password': `Le mot de passe doit contenir au moins 6 caractères`,
    'auth/email-already-in-use': `Cette adresse mail est déjà utilisé !`
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
