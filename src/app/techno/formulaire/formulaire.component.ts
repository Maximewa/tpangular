import { BaseComponent } from './../../base/base.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, Inject, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent extends BaseComponent implements OnInit {
  current_id = "";

  formulaire = new FormGroup({
    name: new FormControl(),
    rate: new FormControl(),
    description: new FormControl(),
  });

  constructor(private _snackbar: MatSnackBar, private firestore: AngularFirestore, @Inject(MAT_DIALOG_DATA) public data: any) {
    super(_snackbar);
    if( data != null){
      this.formulaire.get('name').setValue(data.name);
      this.formulaire.get('rate').setValue(data.rate);
      this.formulaire.get('description').setValue(data.description);
      console.log( data.id );
      this.current_id = data.id;
    }
  }

  ngOnInit(): void {
  }

  valider() {
    if(this.current_id == ""){
      this.firestore.collection('techno').add({
        name: this.formulaire.get('name').value,
        rate: this.formulaire.get('rate').value,
        description: this.formulaire.get('description').value,
      }).then(
        () => {
          this.afficherSnackbar('Techno enregistré !!!');
        }
      )
    }
    else{
      this.firestore.collection("techno").doc(""+this.current_id).update({
        name: this.formulaire.get('name').value,
        rate: this.formulaire.get('rate').value,
        description: this.formulaire.get('description').value,
      }).then(
        () => {
          this.afficherSnackbar('Techno mise à jour !!!');
        }
      )
    }
  }
}
