import { BaseComponent } from './../base/base.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { FormulaireComponent } from './formulaire/formulaire.component';

export interface Techno {
  id: string;
  name: string;
  description: string;
  rate: number;
}
@Component({
  selector: 'app-techno',
  templateUrl: './techno.component.html',
  styleUrls: ['./techno.component.css']
})
export class TechnoComponent extends BaseComponent implements OnInit {
  techno: Observable<any>;

  constructor(
    public snackBar: MatSnackBar,
    public dialog: MatDialog,
    private firestore: AngularFirestore
  ) {
    super(snackBar);
  }


  ngOnInit(): void {
    this.techno = this.firestore.collection('techno').valueChanges({ idField: 'id' });
  }

  ajouter() {
    this.dialog.open(FormulaireComponent, { width: '600px' });
  }

  modifier(techno) {
    this.dialog.open(FormulaireComponent, { width: '600px', data: techno });
  }

  supprimer(techno: Techno) {
    this.firestore.collection('techno').doc(techno.id).delete().then(
      () => {
        this.afficherSnackbar('Techno supprimé !!!');
      }
    )
  }
}
